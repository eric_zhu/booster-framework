package com.booster.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@MappedSuperclass
@EntityListeners(EntityTimeListener.class)
public abstract class BaseEntity<ID_T extends Serializable> implements Serializable {

    private static final long  serialVersionUID         = 1L;

    public static final String CREATED_AT_PROPERTY_NAME = "createdAt";

    public static final String UPDATED_AT_PROPERTY_NAME = "updatedAt";

    public static final String VERSION_PROPERTY_NAME    = "version";

    @Version
    @Column(name = "VERSION")
    private Integer            version;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_AT")
    private Date               createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATED_AT")
    private Date               updatedAt;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isNew() {
        return getId() == null;
    }

    public abstract ID_T getId();
}
