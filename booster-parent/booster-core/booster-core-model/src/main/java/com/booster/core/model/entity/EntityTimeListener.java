package com.booster.core.model.entity;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class EntityTimeListener {

    @PrePersist
    public void prePersist(BaseEntity<?> entity) {
        entity.setCreatedAt(new Date());
        entity.setUpdatedAt(entity.getCreatedAt());
        entity.setVersion(null);
    }

    @PreUpdate
    public void preUpdate(BaseEntity<?> entity) {
        entity.setUpdatedAt(new Date());
    }
}
