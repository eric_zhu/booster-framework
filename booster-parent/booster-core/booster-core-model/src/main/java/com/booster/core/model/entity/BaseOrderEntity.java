package com.booster.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.apache.commons.lang3.builder.CompareToBuilder;

@MappedSuperclass
public abstract class BaseOrderEntity<ID_T extends Serializable> extends BaseEntity<ID_T> {

    private static final long  serialVersionUID    = 1L;

    public static final String ORDER_PROPERTY_NAME = "sortOrder";

    @Column(name = "SORT_ORDER")
    private Long               order;

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public int compareTo(BaseOrderEntity<ID_T> orderEntity) {
        if (orderEntity == null) {
            return 1;
        }
        return new CompareToBuilder().append(getOrder(), orderEntity.getOrder()).append(getId(), orderEntity.getId()).toComparison();
    }

}
