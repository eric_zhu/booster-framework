package com.booster.core.model.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseIdEntity<ID_T extends Serializable> extends BaseEntity<ID_T> {

    private static final long  serialVersionUID = 1L;

    public static final String ID_PROPERTY_NAME = "id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private ID_T               id;

    @Override
    public ID_T getId() {
        return id;
    }

    public void setId(ID_T id) {
        this.id = id;
    }
}
