package com.demo.ecom.core.model.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.booster.core.model.entity.BaseIdEntity;

@Entity
@Table(name = "CATEGORIES")
public class Category extends BaseIdEntity<Long> {

    private static final long serialVersionUID = 1L;

    private String            name;

    private String            description;

    @OneToMany(mappedBy = "category")
    private List<Product>     products;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
