package com.demo.ecom.core.model.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.booster.core.model.entity.BaseIdEntity;

@Entity
@Table(name = "PRODUCTS")
public class Product extends BaseIdEntity<Long> {

    private static final long serialVersionUID = 1L;

    private String            name;

    private String            description;

    public Product() {}

    public Product(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @ManyToOne
    @JoinColumn(name = "CATGORY_ID")
    private Category category;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object that) {
        return EqualsBuilder.reflectionEquals(this, that, "id");
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "id");
    }

}
