package com.demo.ecom.core;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.demo.ecom.core.model.entity.Product;
import com.demo.ecom.core.service.ProductService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { EcomTestContextConfiguration.class })
@Commit
public class IntegrationTest {
    
    @Inject
    ProductService productService;
    
    @Test
    public void testSave() {
        Product product = new Product("Test", "Test");
        productService.save(product);
    }
    
    @Test
    public void testFindAll() {
        System.out.println(productService.findAll().size());
    }
}
