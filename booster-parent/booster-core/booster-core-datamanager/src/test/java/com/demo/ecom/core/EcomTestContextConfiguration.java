package com.demo.ecom.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.demo.ecom.core.configuration.EcomCoreConfiguration;

@PropertySource({ "classpath:persistence.properties" })
@ComponentScan(basePackages = { "com.demo.ecom.core.repository", "com.demo.ecom.core.service" })
@Import({ EcomCoreConfiguration.class })
public class EcomTestContextConfiguration {

}
