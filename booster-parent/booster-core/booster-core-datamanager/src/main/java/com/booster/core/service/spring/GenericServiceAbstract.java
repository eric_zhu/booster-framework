package com.booster.core.service.spring;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.booster.core.model.entity.BaseEntity;
import com.booster.core.query.Filter;
import com.booster.core.query.PageQuery;
import com.booster.core.query.PageResult;
import com.booster.core.query.Sort;
import com.booster.core.repository.GenericRepository;
import com.booster.core.service.GenericService;

@Transactional
public abstract class GenericServiceAbstract<T extends BaseEntity<ID_T>, ID_T extends Serializable> implements GenericService<T, ID_T> {

    private static final String[] UPDATE_IGNORE_PROPERTIES = new String[] { BaseEntity.CREATED_AT_PROPERTY_NAME,
        BaseEntity.UPDATED_AT_PROPERTY_NAME,
        BaseEntity.VERSION_PROPERTY_NAME };

    @Transactional(readOnly = true)
    public T find(ID_T id) {
        return getRepository().find(id);
    }

    @Transactional(readOnly = true)
    public List<T> findAll() {
        return findList(null, null, null, null);
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<T> findList(ID_T... ids) {
        List<T> result = new ArrayList<T>();
        if (ids != null) {
            for (ID_T id : ids) {
                T entity = find(id);
                if (entity != null) {
                    result.add(entity);
                }
            }
        }
        return result;
    }

    @Transactional(readOnly = true)
    public List<T> findList(Integer count, List<Filter> filters, List<Sort> Sorts) {
        return findList(null, count, filters, Sorts);
    }

    @Transactional(readOnly = true)
    public List<T> findList(Integer first, Integer count, List<Filter> filters, List<Sort> Sorts) {
        return getRepository().findList(first, count, filters, Sorts);
    }

    @Transactional(readOnly = true)
    public PageResult<T> findPage(PageQuery PageQuery) {
        return getRepository().findPage(PageQuery);
    }

    @Transactional(readOnly = true)
    public long count() {
        return count(new Filter[] {});
    }

    @Transactional(readOnly = true)
    public long count(Filter... filters) {
        return getRepository().count(filters);
    }

    @Transactional(readOnly = true)
    public boolean exists(ID_T id) {
        return getRepository().find(id) != null;
    }

    @Transactional(readOnly = true)
    public boolean exists(Filter... filters) {
        return getRepository().count(filters) > 0;
    }

    public T save(T entity) {
        Assert.notNull(entity);
        Assert.isTrue(entity.isNew());

        getRepository().persist(entity);
        return entity;
    }

    public T update(T entity) {
        Assert.notNull(entity);
        Assert.isTrue(!entity.isNew());

        if (!getRepository().isManaged(entity)) {
            T persistant = getRepository().find(getRepository().getIdentifier(entity));
            if (persistant != null) {
                copyProperties(entity, persistant, UPDATE_IGNORE_PROPERTIES);
            }
            return persistant;
        }
        return entity;
    }

    public T update(T entity, String... ignoreProperties) {
        Assert.notNull(entity);
        Assert.isTrue(!entity.isNew());
        Assert.isTrue(!getRepository().isManaged(entity));

        T persistant = getRepository().find(getRepository().getIdentifier(entity));
        if (persistant != null) {
            copyProperties(entity, persistant, (String[])ArrayUtils.addAll(ignoreProperties, UPDATE_IGNORE_PROPERTIES));
        }
        return update(persistant);
    }

    public void delete(ID_T id) {
        delete(getRepository().find(id));
    }

    @SuppressWarnings("unchecked")
    public void delete(ID_T... ids) {
        if (ids != null) {
            for (ID_T id : ids) {
                delete(getRepository().find(id));
            }
        }
    }

    public void delete(T entity) {
        if (entity != null) {
            getRepository().remove(getRepository().isManaged(entity) ? entity : getRepository().merge(entity));
        }
    }

    protected void copyProperties(T source, T target, String... ignoreProperties) {
        Assert.notNull(source);
        Assert.notNull(target);

        PropertyDescriptor[] propertyDescriptors = PropertyUtils.getPropertyDescriptors(target);
        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
            String propertyName = propertyDescriptor.getName();
            Method readMethod = propertyDescriptor.getReadMethod();
            Method writeMethod = propertyDescriptor.getWriteMethod();
            if (ArrayUtils.contains(ignoreProperties, propertyName)
                || readMethod == null
                || writeMethod == null
                || !getRepository().isLoaded(source, propertyName)) {
                continue;
            }
            try {
                Object sourceValue = readMethod.invoke(source);
                writeMethod.invoke(target, sourceValue);
            }
            catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    protected abstract GenericRepository<T, ID_T> getRepository();
}
