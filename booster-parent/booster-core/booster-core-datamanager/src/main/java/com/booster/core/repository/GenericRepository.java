package com.booster.core.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.LockModeType;

import com.booster.core.model.entity.BaseEntity;
import com.booster.core.query.Filter;
import com.booster.core.query.PageQuery;
import com.booster.core.query.PageResult;
import com.booster.core.query.Sort;

public interface GenericRepository<T extends BaseEntity<ID_T>, ID_T extends Serializable> {

   public T find(ID_T id);

   public T find(ID_T id, LockModeType lockModeType);

   public List<T> findList(Integer first, Integer count, List<Filter> filters, List<Sort> orders);

   public PageResult<T> findPage(PageQuery pageable);

   public long count(Filter... filters);

   public T persist(T entity);

   public T merge(T entity);

   public void remove(T entity);

   public void refresh(T entity);

   public void refresh(T entity, LockModeType lockModeType);

   public ID_T getIdentifier(T entity);

   public boolean isLoaded(T entity);

   public boolean isLoaded(T entity, String attributeName);

   public boolean isManaged(T entity);

   public void detach(T entity);

   public void lock(T entity, LockModeType lockModeType);

   public void clear();

   public void flush();

}
