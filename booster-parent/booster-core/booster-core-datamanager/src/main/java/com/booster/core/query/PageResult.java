package com.booster.core.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.booster.core.query.Sort.Direction;

public class PageResult<T> implements Serializable {
   
   private static final long serialVersionUID = 1L;

   private final List<T>     content          = new ArrayList<T>();

   private final long        total;

   private final PageQuery   pageable;

   public PageResult() {
      this.total = 0L;
      this.pageable = new PageQuery();
   }

   public PageResult(List<T> content, long total, PageQuery pageable) {
      this.content.addAll(content);
      this.total = total;
      this.pageable = pageable;
   }

   public int getPageNumber() {
      return pageable.getPageNumber();
   }

   public int getPageSize() {
      return pageable.getPageSize();
   }

   public String getSearchProperty() {
      return pageable.getSearchProperty();
   }

   public String getSearchValue() {
      return pageable.getSearchValue();
   }

   public String getOrderProperty() {
      return pageable.getOrderProperty();
   }

   public Direction getOrderDirection() {
      return pageable.getOrderDirection();
   }

   public List<Sort> getOrders() {
      return pageable.getOrders();
   }

   public List<Filter> getFilters() {
      return pageable.getFilters();
   }

   public int getTotalPages() {
      return (int)Math.ceil((double)getTotal() / (double)getPageSize());
   }

   public List<T> getContent() {
      return content;
   }

   public long getTotal() {
      return total;
   }

   public PageQuery getPageable() {
      return pageable;
   }

   public static final <T> PageResult<T> emptyPage(PageQuery pageable) {
      return new PageResult<T>(Collections.<T> emptyList(), 0L, pageable);
   }
}
