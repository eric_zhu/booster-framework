package com.booster.core.service;

import java.io.Serializable;
import java.util.List;

import com.booster.core.model.entity.BaseEntity;

public interface GenericService<T extends BaseEntity<ID_T>, ID_T extends Serializable> {
    public T find(ID_T id);

    public List<T> findAll();

    @SuppressWarnings("unchecked")
    public List<T> findList(ID_T... ids);

    public long count();

    public boolean exists(ID_T id);

    public T save(T entity);

    public T update(T entity);

    public T update(T entity, String... ignoreProperties);

    public void delete(ID_T id);

    @SuppressWarnings("unchecked")
    public void delete(ID_T... ids);

    public void delete(T entity);
}
