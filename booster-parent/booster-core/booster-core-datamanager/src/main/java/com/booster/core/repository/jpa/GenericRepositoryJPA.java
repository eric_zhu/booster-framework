package com.booster.core.repository.jpa;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.booster.core.model.entity.BaseEntity;
import com.booster.core.model.entity.BaseOrderEntity;
import com.booster.core.query.Filter;
import com.booster.core.query.PageQuery;
import com.booster.core.query.PageResult;
import com.booster.core.query.Sort;
import com.booster.core.repository.GenericRepository;

public abstract class GenericRepositoryJPA<T extends BaseEntity<ID_T>, ID_T extends Serializable> implements GenericRepository<T, ID_T> {

   private static final String PROPERTY_SEPARATOR = ".";

   protected Class<ID_T>       idClass;

   protected Class<T>          entityClass;

   @PersistenceContext
   protected EntityManager     entityManager;

   @SuppressWarnings({ "unchecked", "rawtypes" })
   protected GenericRepositoryJPA() {
      Type genericSuperclass = this.getClass().getGenericSuperclass();
      while (!(genericSuperclass instanceof ParameterizedType)) {
         if (!(genericSuperclass instanceof Class))
            throw new IllegalStateException("Unable to determine type " + "arguments because generic superclass neither " + "parameterized type nor class.");
         if (genericSuperclass == GenericRepositoryJPA.class)
            throw new IllegalStateException("Unable to determine type " + "arguments because no parameterized generic superclass " + "found.");
         genericSuperclass = ((Class)genericSuperclass).getGenericSuperclass();
      }
      ParameterizedType type = (ParameterizedType)genericSuperclass;
      Type[] arguments = type.getActualTypeArguments();
      this.entityClass = (Class<T>)arguments[0];
      this.idClass = (Class<ID_T>)arguments[1];
      
   }

   public T find(ID_T id) {
      if (id == null) {
         return null;
      }
      return entityManager.find(entityClass, id);
   }

   public T find(ID_T id, LockModeType lockModeType) {
      if (id == null) {
         return null;
      }
      if (lockModeType != null) {
         return entityManager.find(entityClass, id, lockModeType);
      }
      else {
         return entityManager.find(entityClass, id);
      }
   }

   public List<T> findList(Integer first, Integer count, List<Filter> filters, List<Sort> orders) {
      CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
      criteriaQuery.select(criteriaQuery.from(entityClass));
      return findList(criteriaQuery, first, count, filters, orders);
   }

   public PageResult<T> findPage(PageQuery pageable) {
      CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
      criteriaQuery.select(criteriaQuery.from(entityClass));
      return findPage(criteriaQuery, pageable);
   }

   public long count(Filter... filters) {
      CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
      criteriaQuery.select(criteriaQuery.from(entityClass));
      return count(criteriaQuery, ArrayUtils.isNotEmpty(filters) ? Arrays.asList(filters) : null);
   }

   public T persist(T entity) {
      Assert.notNull(entity);
      entityManager.persist(entity);
      return entity;
   }

   public T merge(T entity) {
      Assert.notNull(entity);

      return entityManager.merge(entity);
   }

   public void remove(T entity) {
      if (entity != null) {
         entityManager.remove(entity);
      }
   }

   public void refresh(T entity) {
      if (entity != null) {
         entityManager.refresh(entity);
      }
   }

   public void refresh(T entity, LockModeType lockModeType) {
      if (entity != null) {
         if (lockModeType != null) {
            entityManager.refresh(entity, lockModeType);
         }
         else {
            entityManager.refresh(entity);
         }
      }
   }

   @SuppressWarnings("unchecked")
   public ID_T getIdentifier(T entity) {
      Assert.notNull(entity);

      return (ID_T)entityManager.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);
   }

   public boolean isLoaded(T entity) {
      Assert.notNull(entity);

      return entityManager.getEntityManagerFactory().getPersistenceUnitUtil().isLoaded(entity);
   }

   public boolean isLoaded(T entity, String attributeName) {
      Assert.notNull(entity);
      Assert.hasText(attributeName);

      return entityManager.getEntityManagerFactory().getPersistenceUnitUtil().isLoaded(entity, attributeName);
   }

   public boolean isManaged(T entity) {
      Assert.notNull(entity);

      return entityManager.contains(entity);
   }

   public void detach(T entity) {
      if (entity != null) {
         entityManager.detach(entity);
      }
   }

   public void lock(T entity, LockModeType lockModeType) {
      if (entity != null && lockModeType != null) {
         entityManager.lock(entity, lockModeType);
      }
   }

   public void clear() {
      entityManager.clear();
   }

   public void flush() {
      entityManager.flush();
   }

   protected List<T> findList(CriteriaQuery<T> criteriaQuery, Integer first, Integer count, List<Filter> filters, List<Sort> orders) {
      Assert.notNull(criteriaQuery);
      Assert.notNull(criteriaQuery.getSelection());
      Assert.notEmpty(criteriaQuery.getRoots());

      CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      Root<T> root = getRoot(criteriaQuery);

      Predicate restrictions = criteriaQuery.getRestriction() != null ? criteriaQuery.getRestriction() : criteriaBuilder.conjunction();
      restrictions = criteriaBuilder.and(restrictions, toPredicate(root, filters));
      criteriaQuery.where(restrictions);

      List<javax.persistence.criteria.Order> orderList = new ArrayList<javax.persistence.criteria.Order>();
      orderList.addAll(criteriaQuery.getOrderList());
      orderList.addAll(toOrders(root, orders));
      if (orderList.isEmpty()) {
         if (BaseOrderEntity.class.isAssignableFrom(entityClass)) {
            orderList.add(criteriaBuilder.asc(getPath(root, BaseOrderEntity.ORDER_PROPERTY_NAME)));
         }
         else {
            orderList.add(criteriaBuilder.desc(getPath(root, BaseOrderEntity.CREATED_AT_PROPERTY_NAME)));
         }
      }
      criteriaQuery.orderBy(orderList);

      TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
      if (first != null) {
         query.setFirstResult(first);
      }
      if (count != null) {
         query.setMaxResults(count);
      }
      return query.getResultList();
   }

   protected PageResult<T> findPage(CriteriaQuery<T> criteriaQuery, PageQuery pageable) {
      Assert.notNull(criteriaQuery);
      Assert.notNull(criteriaQuery.getSelection());
      Assert.notEmpty(criteriaQuery.getRoots());

      if (pageable == null) {
         pageable = new PageQuery();
      }

      CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      Root<T> root = getRoot(criteriaQuery);

      Predicate restrictions = criteriaQuery.getRestriction() != null ? criteriaQuery.getRestriction() : criteriaBuilder.conjunction();
      restrictions = criteriaBuilder.and(restrictions, toPredicate(root, pageable.getFilters()));
      String searchProperty = pageable.getSearchProperty();
      String searchValue = pageable.getSearchValue();
      if (StringUtils.isNotEmpty(searchProperty) && StringUtils.isNotEmpty(searchValue)) {
         Path<String> searchPath = getPath(root, searchProperty);
         if (searchPath != null) {
            restrictions = criteriaBuilder.and(restrictions, criteriaBuilder.like(searchPath, "%" + searchValue + "%"));
         }
      }
      criteriaQuery.where(restrictions);

      List<javax.persistence.criteria.Order> orderList = new ArrayList<javax.persistence.criteria.Order>();
      orderList.addAll(criteriaQuery.getOrderList());
      orderList.addAll(toOrders(root, pageable.getOrders()));
      String orderProperty = pageable.getOrderProperty();
      Sort.Direction orderDirection = pageable.getOrderDirection();
      if (StringUtils.isNotEmpty(orderProperty) && orderDirection != null) {
         Path<?> orderPath = getPath(root, orderProperty);
         if (orderPath != null) {
            switch (orderDirection) {
               case asc:
                  orderList.add(criteriaBuilder.asc(orderPath));
                  break;
               case desc:
                  orderList.add(criteriaBuilder.desc(orderPath));
                  break;
            }
         }
      }
      if (orderList.isEmpty()) {
         if (BaseOrderEntity.class.isAssignableFrom(entityClass)) {
            orderList.add(criteriaBuilder.asc(getPath(root, BaseOrderEntity.ORDER_PROPERTY_NAME)));
         }
         else {
            orderList.add(criteriaBuilder.desc(getPath(root, BaseOrderEntity.CREATED_AT_PROPERTY_NAME)));
         }
      }
      criteriaQuery.orderBy(orderList);

      long total = count(criteriaQuery, null);
      int totalPages = (int)Math.ceil((double)total / (double)pageable.getPageSize());
      if (totalPages < pageable.getPageNumber()) {
         pageable.setPageNumber(totalPages);
      }
      TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
      query.setFirstResult((pageable.getPageNumber() - 1) * pageable.getPageSize());
      query.setMaxResults(pageable.getPageSize());
      return new PageResult<T>(query.getResultList(), total, pageable);
   }

   protected Long count(CriteriaQuery<T> criteriaQuery, List<Filter> filters) {
      Assert.notNull(criteriaQuery);
      Assert.notNull(criteriaQuery.getSelection());
      Assert.notEmpty(criteriaQuery.getRoots());

      CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      Root<T> root = getRoot(criteriaQuery);

      Predicate restrictions = criteriaQuery.getRestriction() != null ? criteriaQuery.getRestriction() : criteriaBuilder.conjunction();
      restrictions = criteriaBuilder.and(restrictions, toPredicate(root, filters));
      criteriaQuery.where(restrictions);

      CriteriaQuery<Long> countCriteriaQuery = criteriaBuilder.createQuery(Long.class);
      for (Root<?> r : criteriaQuery.getRoots()) {
         Root<?> dest = countCriteriaQuery.from(r.getJavaType());
         dest.alias(getAlias(r));
         copyJoins(r, dest);
      }

      Root<?> countRoot = getRoot(countCriteriaQuery, criteriaQuery.getResultType());
      if (criteriaQuery.isDistinct()) {
         countCriteriaQuery.select(criteriaBuilder.countDistinct(countRoot));
      }
      else {
         countCriteriaQuery.select(criteriaBuilder.count(countRoot));
      }

      if (criteriaQuery.getGroupList() != null) {
         countCriteriaQuery.groupBy(criteriaQuery.getGroupList());
      }
      if (criteriaQuery.getGroupRestriction() != null) {
         countCriteriaQuery.having(criteriaQuery.getGroupRestriction());
      }
      if (criteriaQuery.getRestriction() != null) {
         countCriteriaQuery.where(criteriaQuery.getRestriction());
      }
      return entityManager.createQuery(countCriteriaQuery).getSingleResult();
   }

   private String getAlias(Selection<?> selection) {
      if (selection == null) {
         return null;
      }
      String alias = selection.getAlias();
      if (alias == null) {
         alias = generateAlias();
         selection.alias(alias);
      }
      return alias;
   }

   private String generateAlias() {
      StringBuilder alias = new StringBuilder();
      alias.append(entityClass.getSimpleName()).append(System.currentTimeMillis()).append(new Random().nextInt(1000));
      return alias.toString();
   }

   private Root<T> getRoot(CriteriaQuery<T> criteriaQuery) {
      if (criteriaQuery == null) {
         return null;
      }
      return getRoot(criteriaQuery, criteriaQuery.getResultType());
   }

   private Root<T> getRoot(CriteriaQuery<?> criteriaQuery, Class<T> clazz) {
      if (criteriaQuery == null || CollectionUtils.isEmpty(criteriaQuery.getRoots()) || clazz == null) {
         return null;
      }
      for (Root<?> root : criteriaQuery.getRoots()) {
         if (clazz.equals(root.getJavaType())) {
            return (Root<T>)root.as(clazz);
         }
      }
      return null;
   }

   @SuppressWarnings("unchecked")
   private <X> Path<X> getPath(Path<?> path, String propertyPath) {
      if (path == null || StringUtils.isEmpty(propertyPath)) {
         return (Path<X>)path;
      }
      String property = StringUtils.substringBefore(propertyPath, PROPERTY_SEPARATOR);
      return getPath(path.get(property), StringUtils.substringAfter(propertyPath, PROPERTY_SEPARATOR));
   }

   private void copyJoins(From<?, ?> from, From<?, ?> to) {
      for (Join<?, ?> join : from.getJoins()) {
         Join<?, ?> toJoin = to.join(join.getAttribute().getName(), join.getJoinType());
         toJoin.alias(getAlias(join));
         copyJoins(join, toJoin);
      }
      for (Fetch<?, ?> fetch : from.getFetches()) {
         Fetch<?, ?> toFetch = to.fetch(fetch.getAttribute().getName());
         copyFetches(fetch, toFetch);
      }
   }

   private void copyFetches(Fetch<?, ?> from, Fetch<?, ?> to) {
      for (Fetch<?, ?> fetch : from.getFetches()) {
         Fetch<?, ?> toFetch = to.fetch(fetch.getAttribute().getName());
         copyFetches(fetch, toFetch);
      }
   }

   @SuppressWarnings("unchecked")
   private Predicate toPredicate(Root<T> root, List<Filter> filters) {
      CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      Predicate restrictions = criteriaBuilder.conjunction();
      if (root == null || CollectionUtils.isEmpty(filters)) {
         return restrictions;
      }
      for (Filter filter : filters) {
         if (filter == null) {
            continue;
         }
         String property = filter.getProperty();
         Filter.Operator operator = filter.getOperator();
         Object value = filter.getValue();
         Boolean ignoreCase = filter.getIgnoreCase();
         Path<?> path = getPath(root, property);
         if (path == null) {
            continue;
         }
         switch (operator) {
            case eq:
               if (value != null) {
                  if (BooleanUtils.isTrue(ignoreCase) && String.class.isAssignableFrom(path.getJavaType()) && value instanceof String) {
                     restrictions = criteriaBuilder.and(restrictions,
                        criteriaBuilder.equal(criteriaBuilder.lower((Path<String>)path), ((String)value).toLowerCase()));
                  }
                  else {
                     restrictions = criteriaBuilder.and(restrictions, criteriaBuilder.equal(path, value));
                  }
               }
               else {
                  restrictions = criteriaBuilder.and(restrictions, path.isNull());
               }
               break;
            case ne:
               if (value != null) {
                  if (BooleanUtils.isTrue(ignoreCase) && String.class.isAssignableFrom(path.getJavaType()) && value instanceof String) {
                     restrictions = criteriaBuilder.and(restrictions,
                        criteriaBuilder.notEqual(criteriaBuilder.lower((Path<String>)path), ((String)value).toLowerCase()));
                  }
                  else {
                     restrictions = criteriaBuilder.and(restrictions, criteriaBuilder.notEqual(path, value));
                  }
               }
               else {
                  restrictions = criteriaBuilder.and(restrictions, path.isNotNull());
               }
               break;
            case gt:
               if (Number.class.isAssignableFrom(path.getJavaType()) && value instanceof Number) {
                  restrictions = criteriaBuilder.and(restrictions, criteriaBuilder.gt((Path<Number>)path, (Number)value));
               }
               break;
            case lt:
               if (Number.class.isAssignableFrom(path.getJavaType()) && value instanceof Number) {
                  restrictions = criteriaBuilder.and(restrictions, criteriaBuilder.lt((Path<Number>)path, (Number)value));
               }
               break;
            case ge:
               if (Number.class.isAssignableFrom(path.getJavaType()) && value instanceof Number) {
                  restrictions = criteriaBuilder.and(restrictions, criteriaBuilder.ge((Path<Number>)path, (Number)value));
               }
               break;
            case le:
               if (Number.class.isAssignableFrom(path.getJavaType()) && value instanceof Number) {
                  restrictions = criteriaBuilder.and(restrictions, criteriaBuilder.le((Path<Number>)path, (Number)value));
               }
               break;
            case like:
               if (String.class.isAssignableFrom(path.getJavaType()) && value instanceof String) {
                  if (BooleanUtils.isTrue(ignoreCase)) {
                     restrictions = criteriaBuilder.and(restrictions,
                        criteriaBuilder.like(criteriaBuilder.lower((Path<String>)path), ((String)value).toLowerCase()));
                  }
                  else {
                     restrictions = criteriaBuilder.and(restrictions, criteriaBuilder.like((Path<String>)path, (String)value));
                  }
               }
               break;
            case in:
               restrictions = criteriaBuilder.and(restrictions, path.in(value));
               break;
            case isNull:
               restrictions = criteriaBuilder.and(restrictions, path.isNull());
               break;
            case isNotNull:
               restrictions = criteriaBuilder.and(restrictions, path.isNotNull());
               break;
         }
      }
      return restrictions;
   }

   private List<javax.persistence.criteria.Order> toOrders(Root<T> root, List<Sort> orders) {
      List<javax.persistence.criteria.Order> orderList = new ArrayList<javax.persistence.criteria.Order>();
      if (root == null || CollectionUtils.isEmpty(orders)) {
         return orderList;
      }
      CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
      for (Sort order : orders) {
         if (order == null) {
            continue;
         }
         String property = order.getProperty();
         Sort.Direction direction = order.getDirection();
         Path<?> path = getPath(root, property);
         if (path == null || direction == null) {
            continue;
         }
         switch (direction) {
            case asc:
               orderList.add(criteriaBuilder.asc(path));
               break;
            case desc:
               orderList.add(criteriaBuilder.desc(path));
               break;
         }
      }
      return orderList;
   }
}