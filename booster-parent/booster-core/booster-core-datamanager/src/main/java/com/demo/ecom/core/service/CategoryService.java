package com.demo.ecom.core.service;

import com.booster.core.service.GenericService;
import com.demo.ecom.core.model.entity.Category;

public interface CategoryService extends GenericService<Category, Long>{

}
