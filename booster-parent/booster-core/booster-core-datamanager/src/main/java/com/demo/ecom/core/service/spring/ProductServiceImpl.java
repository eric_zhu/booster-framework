package com.demo.ecom.core.service.spring;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.booster.core.repository.GenericRepository;
import com.booster.core.service.spring.GenericServiceAbstract;
import com.demo.ecom.core.model.entity.Product;
import com.demo.ecom.core.repository.ProductRepository;
import com.demo.ecom.core.service.ProductService;

@Service
public class ProductServiceImpl extends GenericServiceAbstract<Product, Long> implements ProductService {

    @Inject
    private ProductRepository productRepository;

    @Override
    protected GenericRepository<Product, Long> getRepository() {
        return productRepository;
    }

}
