package com.demo.ecom.core.repository.jpa;

import org.springframework.stereotype.Repository;

import com.booster.core.repository.jpa.GenericRepositoryJPA;
import com.demo.ecom.core.model.entity.Product;
import com.demo.ecom.core.repository.ProductRepository;

@Repository
public class ProductRepositoryJPA extends GenericRepositoryJPA<Product, Long> implements ProductRepository{

}
