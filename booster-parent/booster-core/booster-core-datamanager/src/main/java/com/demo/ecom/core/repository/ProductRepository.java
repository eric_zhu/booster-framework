package com.demo.ecom.core.repository;

import com.booster.core.repository.GenericRepository;
import com.demo.ecom.core.model.entity.Product;

public interface ProductRepository extends GenericRepository<Product, Long> {

}
