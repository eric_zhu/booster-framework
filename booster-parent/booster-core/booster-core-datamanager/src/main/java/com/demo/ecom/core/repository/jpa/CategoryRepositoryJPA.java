package com.demo.ecom.core.repository.jpa;

import org.springframework.stereotype.Repository;

import com.booster.core.repository.jpa.GenericRepositoryJPA;
import com.demo.ecom.core.model.entity.Category;
import com.demo.ecom.core.repository.CategoryRepository;

@Repository
public class CategoryRepositoryJPA extends GenericRepositoryJPA<Category, Long> implements CategoryRepository {

}
