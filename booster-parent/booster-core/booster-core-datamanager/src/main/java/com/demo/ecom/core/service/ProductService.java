package com.demo.ecom.core.service;

import com.booster.core.service.GenericService;
import com.demo.ecom.core.model.entity.Product;

public interface ProductService extends GenericService<Product, Long>{

}
