package com.demo.ecom.core.repository;

import com.booster.core.repository.GenericRepository;
import com.demo.ecom.core.model.entity.Category;

public interface CategoryRepository extends GenericRepository<Category, Long> {

}
