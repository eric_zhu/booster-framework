package com.demo.ecom.core.service.spring;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.booster.core.repository.GenericRepository;
import com.booster.core.service.spring.GenericServiceAbstract;
import com.demo.ecom.core.model.entity.Category;
import com.demo.ecom.core.repository.CategoryRepository;

@Service
@Transactional
public class CategoryServiceImpl extends GenericServiceAbstract<Category, Long> {

    @Inject
    private CategoryRepository categoryRepository;
    
    @Override
    protected GenericRepository<Category, Long> getRepository() {
        return categoryRepository;
    }
}
