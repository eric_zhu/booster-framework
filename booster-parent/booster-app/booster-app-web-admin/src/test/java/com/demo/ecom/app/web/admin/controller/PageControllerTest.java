package com.demo.ecom.app.web.admin.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.view.InternalResourceView;

public class PageControllerTest {

    @Test
    public void testToPage() throws Exception {
        PageController pageController = new PageController();
        MockMvc mockMvc = standaloneSetup(pageController).setSingleView(new InternalResourceView("/WEB-INF/jsps/hello.jsp")).build();
        mockMvc.perform(get("/page/hello")).andExpect(view().name("hello"));
    }
}
